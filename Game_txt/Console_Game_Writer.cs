using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Game_txt
{
    public static class GameTextWriter // класс для работы с консолью 
    {
        private static int speed_dialog = 50; // скорость вывода текста в консоль 
        private static int speed_text = 20; // скорость механического вывода
        private static int speed_mech_message = 70; // скорость механического сообщения
        public static int Speed_dialog
        {
            get => speed_dialog;
        }

        public static string SetSpeed(int value) //метод для изменения скорости 
        {
            if (value < 0) // проверка на отрицательное число( скорость)  
            {
                return "Показатель скорости не может быть отрицательным.\n" +
                    $"Текущая скорость: {speed_dialog}.";
            }
            else
            {
                speed_dialog = value;
                return "Показатель скорости был успешно изменен.\n" +
                    $"Текущая скорость: {speed_dialog}.";
            }
        }

        public static void Dialog_Writer(string str) //метод для вывода текста Диалога в консоль
        {
            for (int i = 0; i < str.Length; i++)
            {
                Console.Write(str[i]);
                //Console.Beep( 659, 300);
                Thread.Sleep(speed_dialog);
            }
            Console.WriteLine("");
        }
        public static void Mechanical_messages(string[] str) // Метод для вывода основных сообщений
        {
            for (int i = 0; i < str.Length; i++)
            {
                Console.WriteLine(str[i]);
                Thread.Sleep(speed_mech_message);
            }
        }
        public static void Mechanical_writer(string str) // Метод для вывода основных сообщений
        {
            foreach (char el in str)
            {
                Console.Write(el);
                Thread.Sleep(speed_text);
            }
            Console.WriteLine();
            //for (int i = 0; i < str.Length; i++)
            //{
            //    Console.WriteLine(str[i]);
            //    Thread.Sleep(speed_mech_message);
            //}
        }
    }


    public static class Game_Mechanical_Message // класс с техническими сообщениями,
    {
        private static string[] first_setting_message = new string[] //Окно настроек
        {
            "",
            "=========================================|SETTINGS|=========================================",
            "",
            $"Текущая скорость воспроизведения диалогов: {GameTextWriter.Speed_dialog}",
            "",
            "============================================================================================",
            ""

        };

        private static string[] second_setting_message = new string[] //Диалог настроек
        {
            "Вы хотите изменить скорость?",
            "Введите 'Yes' / 'No' "
        };

        private static string[] game_name = new string[]
        {
           "                       ╔▓▒╢╗                                 ",
           "                      ▐▓╢╣╣╣                                 ",
           "                     ╢╫▓╢╣▒▒▒╥                               ",
           "                    ╫▒╢╢╢╢▒▒▒▒▒▒╥                            ",
           "                  ╓╣▒▒╢╣╢╣╣▒▒▒▒▒▒▒║╖                         ",
           "                 ╥▒▒▒▒▒╢╢╣▒▒▒▒▒▒▒▒▒▒▒▒╖                      ",
           "                ╢▒▒▒▒╢▒╫╣╣╢▒▒░▒▒▒▒▒▒▒▒▒▒╖                    ",
           "              ╓╢▒▒▒╢▒▒▒╢╣▓╣▒▒▒▒▒▒▒▒▒╢▒▒▒▒▒╖                  ",
           "             ╓╢▒▒▒╢╢╢▒▒╢╫▓▓▓╣▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒                 ",
           "             ╢▒▒▒▒╫╣╣╣▒▒╢▓╫╢▓╣▒▒▒▒╢╢▒▒▒▒▒▒▒▒▒╖               ",
           "            ║▒▒╢╣╢╢╢╣╣╣╢╢╢▓╣▒▒▒▒▒▒▒╣▒▒▒▒▒▒▒▒▒▒╖              ",
           "           ║▒▒▒╢╢╢╢╣╢╢╫▓▓╢╬╣╢╣╢▒▒╢╢▒▒▒▒▒▒▒▒▒▒▒▒▒             ",
           "          ╓▒▒▒╢╣╢╣╢╣╣▓▓╢╣╣▓▓▓▓▓▓▓╣╫▓╣╣╫▓╣▒▒▒▒▒▒▒▒╖           ",
           "          ║╢╢╢╣╣╣╫╣╣╢▓╣▓╣╢╫▓▓▓▓▓▓▓▓▓▓▓╣╣╫╣▒╢╣▒▒▒▒▒╖          ",
           "         ╓▒╢╢╢╣╢▒▒▒▓▓▒╢▓▓▓▓▓▓╣╢╫╣╣╢╫╣▓▓▓▓▓▓╣╢▓╢▒╢▓▒╣╖        ",
           "         ╢▒▒▓▓╣▓▓▓▓▓▓▓╢▓▓▓▓▓╢╢╣╢╣╣╣╣╢╢╢▒▒╣▓▓▓▓▓╣╢╣▓╣╬▒║      ",
           "         ▒▓╣╣╢▓▓▓╢▓▓▓▓▓▓▓▓▓▓╬╫╫▓╣╣╣╣╢╢▒▒▒╣▒▒▒▒▒╢╫╬▓╣╣╣╝      ",
           "        ║▒▒╢▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓╣╢╣╢╢╢╢╢╢╣▒▒▒▒╢╢╝╜               ",
           "       ╓╫▓▓▓▓▓╢▓▓▓▓▓▓╣▓╢╫╣╣▓╣╣╣╢╣╢╣╢╢Ñ╝╜╙                    ",
           "      ▓▓▓▓▓▓▓╩╩▀╩╩╩╩╩╩╩╩╩╩╨╜╙╙╙                              ",
           "      ▀▓▓▓▓                                                  ",
           "                                                             ", 
           "░█▀▀▀ ░█▀▀█ ░█─░█ ░█▀▀█ ░█▀▀▀█ ░█▀▀█ ░█─░█ ░█▀▄▀█ ─█▀▀█ ░█─▄▀", 
           "░█▀▀▀ ░█─── ░█▀▀█ ░█▄▄█ ░█──░█ ░█─── ░█▀▀█ ░█░█░█ ░█▄▄█ ░█▀▄─", 
           "░█▄▄▄ ░█▄▄█ ░█─░█ ░█─── ░█▄▄▄█ ░█▄▄█ ░█─░█ ░█──░█ ░█─░█ ░█─░█",
           "                                                             ",
           "============================================================="
        };

        private static string[] message_of_ready = new string[] //Диалог старта
        {
            "Вы готовы начать?",
            "Введите слово 'ready'."
        };
        private static string incorrect_response = "Введен некорректный ответ"; // при вводе неверного ответа 

        private static string incorrect_answer_ready = "Мы сомневаемся в вашей готовности."; // при вводе неверного слова готовности

        private static string separator = "============================================================================================";
        private static string separator2 = "--------------------------------------------------------------------------------------------";

        public static string Separator
        {
            get => separator;
        }
        public static string Separator2
        {
            get => separator2;
        }

        private static string[] for_help = new string[]
        {
            "Чтобы изменить скорость диалогов",
            "в окне выбора ответа введите 'setting'."
        };
        private static string[] end_the_game = new string[]
        {
            "Игра окончена",
            "",
            "Вы хотите начать сначала?",
            "Введите 'нет' - N / 'Да' - Y"
        };
        public static string[] For_Help
        {
            get => for_help;
        }
        public static string Incorrect_response
        {
            get => incorrect_response;
        }

        public static string Incorrect_answer_ready
        {
            get => incorrect_answer_ready;
        }


        public static void Give_Setting() //Метод для вывода настроек
        {
            GameTextWriter.Mechanical_messages(first_setting_message);
        }
        public static void Set_settings() // Метод для вывода настроек с диалогом об изменениях
        {
            GameTextWriter.Mechanical_messages(first_setting_message);
            GameTextWriter.Mechanical_messages(second_setting_message);
        }

        public static void Start_Game_Message()
        {
            GameTextWriter.Mechanical_messages(message_of_ready);
        }
        public static void Game_first_init()
        {
            GameTextWriter.Mechanical_messages(game_name);
            GameTextWriter.Mechanical_messages(for_help);
            GameTextWriter.Mechanical_messages(message_of_ready);
        }
        public static void Game_second_init()
        {
            GameTextWriter.Mechanical_messages(for_help);
            GameTextWriter.Mechanical_messages(message_of_ready);
        }
        public static void Game_End()
        {
            GameTextWriter.Mechanical_messages(end_the_game);
        }
    }

}

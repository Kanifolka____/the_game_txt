using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Web;

namespace Game_txt
{
    public class Program
    {
        static void Main(string[] args)
        {
            Start_Game();
        }

        static void Start_Game()
        {
            Game_Mechanical_Message.Game_first_init();
            string str;
            while (true)
            {
                str = Console.ReadLine().ToLower()?.Replace(" ", "");
                if (str == "ready") 
                {
                    Game_Process();
                    break;
                }
                else if (str == "setting")
                {
                    Setting();
                    //break;
                }
                else
                {
                    Console.WriteLine(Game_Mechanical_Message.Incorrect_answer_ready);
                }
            }
        }

        static void Restart_Game()
        {
            Game_Mechanical_Message.Game_End();
            string str;
            while (true)
            {
                str = Console.ReadLine().ToLower()?.Replace(" ", "");
                if (str == "y") 
                {
                    Story_of_the_life.Reset_Story();
                    Game_Process();
                    break;
                }
                else if (str == "n") 
                {
                    Console.WriteLine("\n" + "\n" + "\n" + "\n");
                    break; 
                }
                else if (str == "setting")
                {
                    Setting();
                }
                else
                {
                    Console.WriteLine(Game_Mechanical_Message.Incorrect_answer_ready);
                }
            }
        }

        static void Setting()
        {
            string str;
            void edit_setting() // Вспомогательный метод, отвечающий за изменения скорости диалога
            {
                Console.WriteLine("Введите желаемое значение. \n" +
                    "Рекомендуемая скорость: 50.\n" +
                    "Чтобы выйти напишите 'Exit'.\n");
                while (true)
                {
                    str = Console.ReadLine().ToLower()?.Replace(" ", "");
                    if (str == "exit") { break; }

                    else if (int.TryParse(str, out int x))
                    {
                        Console.WriteLine(GameTextWriter.SetSpeed(x) + "\n");

                        break;
                    }
                    else
                    {
                        Console.WriteLine(Game_Mechanical_Message.Incorrect_response);
                    }
                }
            }
            //Тело самого метода Setting
            Game_Mechanical_Message.Set_settings();
            while (true) //Обрабатываем и создаем "диалоговое окно" с пользователем
            {
                str = Console.ReadLine().ToLower()?.Replace(" ", "");
                if (str == "no") { break; }
                else if (str == "yes")
                {
                    edit_setting();
                    break;
                }
                else { Console.WriteLine(Game_Mechanical_Message.Incorrect_response); }
            }
        }

        static int Answer_2()
        {
            string str;
            while (true)
            {
                str = Console.ReadLine().ToLower()?.Replace(" ", "");
                if (str == "a") { return 1; }
                else if (str == "b") { return 2; }
                else if (str == "setting") { Setting(); }
                else
                {
                    Console.WriteLine(Game_Mechanical_Message.Incorrect_response);
                }
            }
        }
        static int Answer_3()
        {
            string str;
            while (true)
            {
                str = Console.ReadLine().ToLower()?.Replace(" ", "");
                if (str == "a") { return 1; }
                else if (str == "b") { return 2; }
                else if (str == "c") { return 3; }
                else if (str == "setting") { Setting(); }
                else
                {
                    Console.WriteLine(Game_Mechanical_Message.Incorrect_response);
                }
            }
        }
        public static int Answer_4()
        {
            string str;
            while (true)
            {
                str = Console.ReadLine().ToLower()?.Replace(" ", "");
                if (str == "a") { return 1; }
                else if (str == "b") { return 2; }
                else if (str == "c") { return 3; }
                else if (str == "d") { return 4; }
                else if (str == "setting") { Setting(); }
                else
                {
                    Console.WriteLine(Game_Mechanical_Message.Incorrect_response);
                }
            }
        }

        public static void Game_Process()
        {
            int i;
            Console.WriteLine(Game_Mechanical_Message.Separator);

            Story_of_the_life.Dialog_1();
            i = Answer_2();
            Story_of_the_life.Choice_result_1(i);
            if (!Story_of_the_life.Life) 
            {
                Restart_Game();
                return; 
            }

            Story_of_the_life.Dialog_2();
            i = Answer_2();
            Story_of_the_life.Choice_result_2(i);
            if (!Story_of_the_life.Life)
            {
                Restart_Game();
                return; 
            }

            Story_of_the_life.Dialog_3();
            i = Answer_3();
            Story_of_the_life.Choice_result_3(i);
            if (!Story_of_the_life.Life) 
            {
                Restart_Game();
                return; 
            }

            Story_of_the_life.Dialog_4();
            i = Answer_2();
            Story_of_the_life.Choice_result_4(i);
            if (!Story_of_the_life.Life) 
            {
                Restart_Game();
                return;
            }

            Story_of_the_life.Dialog_5();
            i = Answer_3();
            while (i == 1)
            {
                Story_of_the_life.Choice_result_5(i);
                Story_of_the_life.Dialog_5_1();
                i = Answer_3();
            }
            Story_of_the_life.Choice_result_5(i);
            if (!Story_of_the_life.Life) 
            {
                Restart_Game();
                return;
            }

        }

    }


}




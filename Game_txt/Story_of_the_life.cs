using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game_txt
{
    public static class Story_of_the_life
    {
        private static bool first_choice_of_way;
        private static bool things_with_chigan = false; //взаимодействие с цыганом 
        private static bool second_choice_of_way;
        private static bool life = true;
        static Dictionary<int, string> surroundings = new Dictionary<int, string>()
        {
            [1] = "Жил-был старик со старухою. " +
            "Просит старик: \n",
            [2] = "Взяла старуха крылышко, по коробу поскребла, по сусеку помела, и набралось муки пригоршни с две." +
            " \n Замесила на сметане, изжарила в масле и положила на окошечко постудить. " +
            "\n Эчпочмак лежал—лежал и думал о технологическом  предпринимательсве.\n",

            [3] = "А бабка оказалась пиратом бывшим, причем не простым, а Байкальским, была прошарена в стартапах местных. " +
            "\n Посоветовала малому на Байкал отправиться, уму разуму научиться." +
            "\nВзяла старуха крылышко, по коробу поскребла, по сусеку помела, под диваном подмела, деда протрясла," +
            "\n и в итоге нашла 5 тысяч на билет до Иркутска." +
            "\nНа выходе из дома встречает Эчпочмака Цыган, и спрашивает:",

            [4] = "Хочешь приумножить свой капитал?",
            [5] = "Прибывает Эчпочмак в аэропорт. На табло 3 рейса: «Бар», «Школа технологического предпринимательства», «Казанстан».",

            [6] = "Эчпочмак подъезжает к бару на Boeing 747. Заходит он в бар и говорит: ",
            [7] = "Эчпочмак прибывает в Иркутск. Напротив Аэропорта стоит бар, а рядом с баром знакомым Цыган.",
            [8] = "Эчпочмак доковылял до Байкала. Перед ним распутье. \nКамень гласит: направо пойдешь - в город Бабушкин попадешь," +
            " \n прямо пойдешь - к озеру попадешь, а налево пойдешь - в офис Великого Бешбармака попадешь.",
            [9] = "Вы решились пойти к озеру.\n" +
            "Перед Эчпочмаком открывается вид на человека, смотрящего вдаль. Он на половину в воде.\n"+
            "Незнакомец на столько преисполнился в своих познаниях, что не замечает героя.\n Эчпочмак подходит ближе и узнает в нем черты своей бабушки.\n" +
            "Не произнося ни слова, она отвела его в «Школу технологического предпринимательства»,\n где Эчпочмак становится прошаренным бизнесменом в сфере IT технологий (УСПЕШНАЯ КОНЦОВКА)",
            [10] = "Эчпочмак прибывает в город Бабушкин. Жители просят помочь по разным хозяйским делам.",
            [11] = "Перед героем камень: направо пойдешь - в город Бабушкин попадешь, прямо пойдешь - к озеру попадешь, " +
            "\n а налево пойдешь - в офис Великого Бешбармака попадешь.",
            [12] = "Перед героем открывается вид на «Школу технологического предпринимательства». Он решает вернуться к камню.",
            [13] = "Сыграть?",
            [14] = "Сыграть ещё?",
            [15] = "Эчпочмак прибывает в город Бабушкин.\n" +
            "Жители просят помочь по разным хозяйским делам.\n" +
            "В итоге эчпочмак пробыл в городе 2 года и вместо технологического предпринимательства открыл свою ферму,\n где начал печь эчпочмаки."
        };

        static Dictionary<int, string> dialogue = new Dictionary<int, string>()
        {
            [1] = "— Испеки, старуха, эчпочмак!" +
            "\n— Из чего печь — то ? Муки нету, — отвечает ему старуха." +
            "\n— Э — эх, старуха! По коробу поскреби, по сусеку помети; авось муки и наберется.",
            [2] = "– Не хотите ли приумножить свой капитал?",
            [3] = "– Можно мне виски с колой с соком чуть минералки и немного кумыса.",
        };

        static Dictionary<int, string> answers = new Dictionary<int, string>()
        {
            [1] = "Эчпочмака съедает татарин на ужин.",
            [2] = "Эчпочмак решил создать  свое технологичное предприниятие в сфере IT технологий.",
            [3] = "Цыган хитро улыбается Эчпочмаку и провожает его до аэропорта.\n" +
            "Он рассказывает топ 10 интересных фактов о компьютерах.",
            [4] = "У Эчпочмака отобрали 5 тысяч, и его съел Цыган. В Иркутск он больше не едет. Герой опозорил бабку с дедом.",
            [5] = "Эчпочмаку дали напиток. Бармен оборачивается к нему. Эчпочмак признаёт в нём Цыгана. \n Он открывает перед тем ноутбук с лекцией о «Школе технологического предпринимательства».",
            [6] = "Эчпочмак заплатил за напиток.",
            [7] = "Эчпочмак заплатил пятерную цену за напиток.",
            [8] = "Эчпочмак очень понравился Цыгану. Он заплатил за героя.",
            [9] = "Эчпочмака депортировали в Иркутск вместе с цыганским табором.",
            [10] = "– Простите, но мы не обслуживаем эчпочмаки, – отвечает бармен и вышвыривает героя за дверь. Он решает дойти до Иркутска сам. Он наполняется решимостью.",
            [11] = "Эчпочмака избила Монгольское Крымско-татарское иго чебуреков.",
            [12] = "Цыган дает совет ехать на озеро Байкал и найти там Великого Бешбармака. Он спонсирует Эчпочмаку поездку.",
            [13] = "Эчпочмак остался в Иркутске. У него не хватило знаний стать успешным предпринимателем в сфере IT технологий. Эчпочмак решает открыть свой бар и становится цыганом." +
            "Нейтральная концовка",
            [14] = "Герой бродил месяц по Иркутску. У него закончился срок годности, и он не стал успешным предпринимателем в сфере IT технологий. Герой опозорил бабку с дедом.",
            [15] = "Осмотрев всё вокруг, Эчпочмак никого не находит. День закончился. Осмотреть побережье можно ещё раз на следующий день.",
            [16] = "Осмотреть побережье можно ещё раз на следующий день.",
            [17] = "Эчпочмак отбил картошку у колорадских жуков. Бабушка угощает героя зелёным салатом, что восстановил силы. Наступил следующий день.",
            [18] = "Эчпочмак поставил на место завистливых соседей. Пожилая пара похвалила героя и предложила остаться на ночь. Наступил следующий день.",
            [19] = "Эчпочмак с помощью мёда отгоняет мишку Фредди, что напал на деревню. Все ликуют и угощают героя пирожками. Наступил следующий день.",
            [20] = "Эчпочмак помог пасти овец на лугу. Дедушка угостил героя молоком, что восстановил силы. Наступил следующий день.",
            [21] = "Эчпочмак помогает полить огороды. Бабушка связала герою шарф в знак благодарности. Наступил следующий день.",
            [22] = "Игра окончена",
            [23] = "У вас закончились деньги. Игра окончена.",
            [24] = "У вас закончилось здоровье. Игра окончена.",
            [25] = "Цыган предлагает сыграть в монетку. Орёл – Эчпочмак побеждает и получает выигрыш в двойном размере. " +
            "\n Решка – выигрывает Цыган и забирает часть денег.",
            [26] = "Орёл. Эчпочмак забирает свой выигрыш.",
            [27] = "Решка. Цыган усмехнулся и забрал свой приз.",
            [28] = "Цыган пожал плечами и убрал монетку в карман.",
            [29] = "Эчпочмак потерялся в лесу по пути в Казанстан."

        };

        static Dictionary<int, string> questions = new Dictionary<int, string>()
        {
            [1] = "Начать свое технологическое предпринимательство в сфере IT технологий?",
            [2] = "Приумножить свой капитал?",
            [3] = "Куда пойти?",
            [4] = "Вы хотите выпить с цыганом?",
            [5] = "Куда вы отправитесь?"
        };

        private static string splitter = "\n";
        private static string dead = "На этом история Эчпочмака обрывается.\n";

        public static bool Life
        {
            get => life;
        }
        public static void Dialog_1()
        {
            GameTextWriter.Mechanical_writer(surroundings[1]);
            GameTextWriter.Dialog_Writer(dialogue[1]);
            GameTextWriter.Mechanical_writer(surroundings[2]);

            Console.WriteLine(Game_Mechanical_Message.Separator2);
            GameTextWriter.Mechanical_writer(questions[1] + splitter);
            Console.WriteLine("'НЕТ' - A \n" + "'ДА' - B ");
            Console.WriteLine(Game_Mechanical_Message.Separator2);
        }

        public static bool First_choice_of_the_way
        {
            get => first_choice_of_way;
        }
        public static void Choice_result_1(int i)
        {
            if (i == 1)
            {
                GameTextWriter.Mechanical_writer(answers[1] + splitter
                    + dead);
                life = false;
            }
            else
            {
                GameTextWriter.Mechanical_writer(answers[2] + splitter);
            }
        }

        public static void Dialog_2()
        {
            GameTextWriter.Mechanical_writer(surroundings[3]);
            GameTextWriter.Dialog_Writer(surroundings[4]);

            Console.WriteLine(Game_Mechanical_Message.Separator2);
            GameTextWriter.Mechanical_writer(questions[2] + splitter);
            Console.WriteLine("'ДА' - A \n" + "'НЕТ' - B ");
            Console.WriteLine(Game_Mechanical_Message.Separator2);
        }
        public static void Choice_result_2(int i)
        {
            if (i == 1)
            {
                PlayCoinFlip();
                GameTextWriter.Mechanical_writer(answers[3] + splitter);
            }
            else
            {
                GameTextWriter.Mechanical_writer(answers[4] + splitter + dead);
                life = false;
            }
        }
        public static void Dialog_3()
        {
            GameTextWriter.Mechanical_writer(surroundings[5]);

            Console.WriteLine(Game_Mechanical_Message.Separator2);
            GameTextWriter.Mechanical_writer(questions[3] + splitter);
            Console.WriteLine("'Бар' - A" + splitter
                + "'Школа' - B" + splitter
                + "'КАЗАНСТАН' - C");
            Console.WriteLine(Game_Mechanical_Message.Separator2);
        }

        public static void Choice_result_3(int i)
        {
            switch (i)
            {
                case 1:
                    //GameTextWriter.Mechanical_writer(surroundings[6]);
                    first_choice_of_way = true;

                    break;

                case 2:

                    //GameTextWriter.Mechanical_writer(surroundings[7]);
                    first_choice_of_way = false;
                    break;

                case 3:
                    GameTextWriter.Mechanical_writer(answers[29] + splitter + dead);
                    life = false;
                    break;
            }
        }
        public static void Dialog_4()
        {
            if (first_choice_of_way)
            {
                GameTextWriter.Mechanical_writer(surroundings[6]);
                GameTextWriter.Dialog_Writer(dialogue[3]);
                Random random = new Random();
                int i = random.Next(0, 11);
                if (i <= 1) // 10%
                {
                    GameTextWriter.Mechanical_writer(answers[11] + splitter + dead);
                    life = false;
                    return;
                }
                else if (i > 1 && i <= 5) //40%
                {
                    GameTextWriter.Mechanical_writer(answers[10]);
                }
                else //50%
                {
                    things_with_chigan = true; // Удачное взаимодействие с цыганом в баре
                    GameTextWriter.Mechanical_writer(answers[5]);
                    i = random.Next(0, 11);
                    if (i <= 1) //10%
                    {
                        GameTextWriter.Mechanical_writer(answers[8]);
                    }
                    else if (i > 1 && i <= 5)//40%
                    {
                        GameTextWriter.Mechanical_writer(answers[7]);
                    }
                    else //50%
                    {
                        GameTextWriter.Mechanical_writer(answers[6]);
                    }
                }
            }
            GameTextWriter.Mechanical_writer(surroundings[7]);

            Console.WriteLine(Game_Mechanical_Message.Separator2);
            GameTextWriter.Mechanical_writer(questions[4] + splitter);
            Console.WriteLine("'ДА' - A \n" + "'НЕТ' - B ");
            Console.WriteLine(Game_Mechanical_Message.Separator2);
        }
        public static void Choice_result_4(int i)
        {
            switch (i)
            {
                case 1:
                    if (things_with_chigan)
                    {
                        GameTextWriter.Mechanical_writer(answers[12]);
                    }
                    else
                    {
                        GameTextWriter.Mechanical_writer(answers[13]);
                        life = false; //конец игры
                    }
                    break;
                case 2:
                    GameTextWriter.Mechanical_writer(answers[14] + splitter + dead);
                    life = false;// смерть
                    break;
            }
        }

        public static void Dialog_5()
        {
            GameTextWriter.Mechanical_writer(surroundings[8]);
            Console.WriteLine(Game_Mechanical_Message.Separator2);
            GameTextWriter.Mechanical_writer(questions[5] + splitter);
            Console.WriteLine("'Налево' - A" + splitter
                + "'Прямо' - B" + splitter
                + "'Направо' - C");
            Console.WriteLine(Game_Mechanical_Message.Separator2);
        }

        public static void Dialog_5_1()
        {
            Console.WriteLine(Game_Mechanical_Message.Separator2);
            GameTextWriter.Mechanical_writer(questions[5] + splitter);
            Console.WriteLine("'Налево' - A" + splitter
                + "'Прямо' - B" + splitter
                + "'Направо' - C");
            Console.WriteLine(Game_Mechanical_Message.Separator2);
        }

        public static void Choice_result_5(int i)
        {
            switch (i)
            {
                case 1:
                    GameTextWriter.Mechanical_writer(surroundings[12]);
                    break;
                case 2:
                    GameTextWriter.Mechanical_writer(surroundings[9]);
                    life = false; // концовка игры успешная
                    break;
                case 3:
                    GameTextWriter.Mechanical_writer(surroundings[15]);
                    life = false; // концовка игры 
                    break;
            }
        }

        public static void PlayCoinFlip()
        {
            Random random = new Random();
            int randomNumber = random.Next(0, 2); // Генерируем случайное число 0 или 1
            int userChoice = -1; // Инициализируем переменную с некорректным значением

            while (userChoice != 0 && userChoice != 1)
            {
                Console.WriteLine("Выберите орла (0) или решку (1):");
                string userInput = Console.ReadLine();

                if (int.TryParse(userInput, out userChoice))
                {
                    if (userChoice == 0 || userChoice == 1)
                    {
                        break; // Выход из цикла, если пользователь ввел корректное значение
                    }
                }

                Console.WriteLine("Некорректный ввод. Пожалуйста, введите 0 или 1.");
            }

            string message = randomNumber == 0 ? "Орел" : "Решка";
            Console.WriteLine("Выпало: " + message);

            if (userChoice == randomNumber)
            {
                Console.WriteLine("Эчпочмак забирает свой выигрыш.");
            }
            else
            {
                Console.WriteLine("Цыган усмехнулся и забрал свой приз.");       
            }
        }

        public static void Reset_Story()
        {
            things_with_chigan = false;
            life = true;
        }

    }

}

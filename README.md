# The_Game_TXT

Вы играете за Эчпочмака, который захотел заняться технологическим предпринимательством. Вы пройдете тяжелый путь по России в поисках наставника, который поможет создать вам IT стартап.

В игре присутствует интерактивность и выбор. Квест может предоставлять вам возможность принимать решения, которые будут влиять на развитие сюжета и исход истории.

Есть команда setting, с помощью которой можно менять скорость воспроизведения диалогов.
После смерти персонажа можно начать сначала, не закрывая приложение.

Для того, чтобы играть в игру, понадобится установить Microsoft Visual Studio. Сама игра запускается файлом Game_txt.sln.
